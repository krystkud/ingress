*** Settings ***
Documentation  Test suite dedicated to test examples provided by module.
...            This suite utilizes examples, deploys them one by one and verify it's creation via AWS API
...            Verification is also done for complete removal of the example module
...            Example module provides AWS MVP - Minimal Valuable Product, thus it should not be
...            treated as a sustainable source for environment
Metadata       Version     0.1.0

Resource  resources/terraform.robot
Resource  resources/module_tests.robot

*** Variables ***
${PROJECT_NAME}
${example_path}  ${EXECDIR}/examples/alb/simple
${region}        eu-central-1

*** Test Cases ***
Test simple
    Terraform init    ${example_path}
    Terraform apply   ${example_path}
    ${terraform_resp}=  Terraform get json module values  ${example_path}  ingress_alb  aws_lb.ingress
    Set suite variable  ${name}  ${terraform_resp['values']['name']}

    AWSession.spawn  elbv2  ${region}
    ${aws_api_resp}=  AWSession.get  describe_load_balancers
    ${kms_json}=  AWSession.filter_by_value    ${aws_api_resp}  LoadBalancerName  ${name}
    Should not be empty  ${kms_json}

Test simple-destroy
    Terraform destroy  ${example_path}
    AWSession.spawn  elbv2  ${region}
    ${aws_api_resp}=  AWSession.get  describe_load_balancers
    ${kms_json}=  AWSession.filter_by_value    ${aws_api_resp}  LoadBalancerName  ${name}
    Should be empty  ${kms_json}