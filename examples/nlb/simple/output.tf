output "ingress_80_arn" {
  description = "Return ingress target group arn on port 80"
  value       = module.ingress_nlb.ingress_80_arn
}

output "ingress_443_arn" {
  description = "Return ingress target group arn on port 443"
  value       = module.ingress_nlb.ingress_443_arn
}

output "ingress_dns_name" {
  description = "Return ingress dns name"
  value       = module.ingress_nlb.ingress_dns_name
}

output "ingress_zone_id" {
  description = "Return ingress zone id"
  value       = module.ingress_nlb.ingress_zone_id
}

output "nlb_arn" {
  description = "Return load balancer arn"
  value       = module.ingress_nlb.nlb_arn
}

output "ingress_health_check_sg_id" {
  description = "Return ingress health check security group id."
  value       = module.ingress_nlb.ingress_health_check_sg_id
}
