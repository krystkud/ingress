ACCOUNT_NAME            = "teribu"
AWS_REGION              = "eu-west-2"
DNS_ZONE_NAME           = "ingress-https-alb-module.cat-test.idemia.io"
VPC_CIDR_BLOCK          = "10.10.0.0/16"
PUBLIC_CIDR_BLOCKS      = ["10.10.0.0/24", "10.10.1.0/24", "10.10.2.0/24"]
APPLICATION_CIDR_BLOCKS = ["10.10.10.0/24", "10.10.11.0/24", "10.10.12.0/24"]
