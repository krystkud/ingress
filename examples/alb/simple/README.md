### Creates simple application load balancer dedicated for ingress traffic

> content of the `main.tf` file:

```
provider "aws" {
  region = var.AWS_REGION
}

module "common" {
  source          = "../../../../common"
  ACCOUNT_NAME    = var.ACCOUNT_NAME
  AWS_REGION      = var.AWS_REGION
  ENVIRONMENT     = var.ENVIRONMENT
  PROJECT         = var.PROJECT
  PUBLIC_KEY_PATH = var.PUBLIC_KEY_PATH
}

module "networking" {
  source                  = "../../../../vpc/networking"
  globals                 = module.common.globals
  VPC_CIDR_BLOCK          = var.VPC_CIDR_BLOCK
  PUBLIC_CIDR_BLOCKS      = var.PUBLIC_CIDR_BLOCKS
  APPLICATION_CIDR_BLOCKS = var.APPLICATION_CIDR_BLOCKS
  DATA_CIDR_BLOCKS        = var.DATA_CIDR_BLOCKS
}

module "ingress_alb" {
  source                   = "../../../../ingress/alb"
  globals                  = module.common.globals
  networking               = module.networking.common
  PUBLIC_SUBNET_IDS        = module.networking.public_subnet_ids
  BALANCER_CERTIFICATE_ARN = module.dns_zone.wildcard_certificate_arn
  ENABLE_HTTPS_LISTENER    = false
}

module "dns_zone" {
  source        = "../../../../dns/zone/public"
  globals       = module.common.globals
  DNS_ZONE_NAME = var.DNS_ZONE_NAME
}

```

> content of the `variables.tf` file:

```
variable "AWS_REGION" {
  description = "Provide aws region"
  default     = "eu-central-1"
}

variable "PROJECT" {
  description = "Provide name of the project"
}

variable "ENVIRONMENT" {
  description = "Provide type of the environment, ex dev|test|stg|prod (max 4 characters)"
  default     = "testing"
}

variable "ACCOUNT_NAME" {
  description = "Provide aws account name"
}

variable "DNS_ZONE_NAME" {
  description = "Provide dns zone name"
}

variable "PUBLIC_KEY_PATH" {
  default     = "ubuntu-key.pub"
  description = "Provide path to ssh public key"
}

variable "VPC_CIDR_BLOCK" {
  description = "Provide aws cidr block for whole vpc"
  default     = "10.1.0.0/24"
}

variable "PUBLIC_CIDR_BLOCKS" {
  default     = []
  description = "Provide list of public cidr blocks per availability zones, ex azX = '10.1.1.0/24'"
  type        = list(string)
}

variable "APPLICATION_CIDR_BLOCKS" {
  default     = []
  description = "Provide list of application cidr blocks per availability zones, ex azX = '10.1.10.0/24'"
  type        = list(string)
}

variable "DATA_CIDR_BLOCKS" {
  default     = []
  description = "Provide list of data cidr blocks per availability zones, ex azX = '10.1.20.0/24'"
  type        = list(string)
}

```

> content of the `terraform.tfvars` file:

```
PROJECT                 = "ingress"
ACCOUNT_NAME            = "teribu"
AWS_REGION              = "eu-west-1"
ENVIRONMENT             = "alb-module"
DNS_ZONE_NAME           = "ingress-alb-module.idemia.io"
VPC_CIDR_BLOCK          = "10.10.0.0/16"
PUBLIC_CIDR_BLOCKS      = ["10.10.0.0/24", "10.10.1.0/24", "10.10.2.0/24"]
APPLICATION_CIDR_BLOCKS = ["10.10.10.0/24", "10.10.11.0/24", "10.10.12.0/24"]

```
