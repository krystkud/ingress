resource "aws_security_group" "ingress_health_check_sg" {
  count       = var.ENABLE_HTTPS_LISTENER == false && var.ENABLE_HTTP_LISTENER == false ? 0 : 1
  name        = "${local.environment_name}-${local.type}${local.deployment_suffix}-nlb-hc"
  vpc_id      = var.networking["VpcId"]
  description = "${local.environment_name}-${local.type}${local.deployment_suffix}-nlb-hc-sg"

  tags = merge(
    local.default_tags,
    map(
      "Name", "${local.environment_name}-${local.type}${local.deployment_suffix}-nlb-hc",
      "Object", "aws_security_group"
    )
  )
}

data "aws_subnet" "public_subnets" {
  count = length(var.PUBLIC_SUBNET_IDS)
  id    = var.PUBLIC_SUBNET_IDS[count.index]
}

resource "aws_security_group_rule" "ingress_80_hc" {
  count             = var.ENABLE_HTTPS_LISTENER == false && var.ENABLE_HTTP_LISTENER == false ? 0 : 1
  description       = "Health check network flow from public subnets."
  type              = "ingress"
  from_port         = var.HEALTH_CHECK_PORT == null ? var.BACKEND_PORT : var.HEALTH_CHECK_PORT
  to_port           = var.HEALTH_CHECK_PORT == null ? var.BACKEND_PORT : var.HEALTH_CHECK_PORT
  protocol          = "tcp"
  cidr_blocks       = data.aws_subnet.public_subnets[*].cidr_block
  security_group_id = aws_security_group.ingress_health_check_sg[0].id
}
