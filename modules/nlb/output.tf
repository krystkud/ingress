output "ingress_80_arn" {
  description = "Return ingress target group arn on port 80"
  value       = aws_lb_target_group.ingress.arn
}

output "ingress_443_arn" {
  description = "Return ingress target group arn on port 443"
  value       = concat(aws_lb_target_group.ingress_https[*].arn, [""])[0]
}

output "ingress_dns_name" {
  description = "Return ingress dns name"
  value       = aws_lb.ingress.dns_name
}

output "ingress_zone_id" {
  description = "Return ingress zone id"
  value       = aws_lb.ingress.zone_id
}

output "nlb_arn" {
  description = "Return load balancer arn"
  value       = aws_lb.ingress.arn
}

output "ingress_health_check_sg_id" {
  description = "Return ingress health check security group id."
  value       = aws_security_group.ingress_health_check_sg.*.id
}
