resource "random_string" "nlb" {
  length  = 6
  special = false
  upper   = false

  keepers = {
    environment_name  = local.environment_name
    deployment_suffix = local.deployment_suffix
    vpc_id            = var.networking["VpcId"]
  }
}

resource "random_string" "https" {
  length  = 6
  special = false
  upper   = false

  keepers = {
    environment_name  = local.environment_name
    deployment_suffix = local.deployment_suffix
    vpc_id            = var.networking["VpcId"]
  }
}

resource "aws_lb" "ingress" {
  name                             = "${local.ingress_name}-${local.type}-nlb${local.deployment_suffix}-${random_string.nlb.result}"
  internal                         = var.INTERNAL
  load_balancer_type               = "network"
  subnets                          = var.PUBLIC_SUBNET_IDS
  enable_deletion_protection       = var.ENABLE_DELETION_PROTECTION
  enable_cross_zone_load_balancing = true

  access_logs {
    enabled = var.ACCESS_LOGS_BUCKET_NAME == "" ? false : true
    bucket  = var.ACCESS_LOGS_BUCKET_NAME
    prefix  = var.ACCESS_LOGS_BUCKET_PREFIX
  }

  tags = merge(
    local.default_tags,
    map(
      "Name", "${local.environment_name}-${local.type}-nlb${local.deployment_suffix}",
      "Object", "aws_lb"
    )
  )
  #checkov:skip=CKV_AWS_91:Logging is optional, it should be added on prod envs
}

##
# Define ingress target group port default 1080
##
resource "aws_lb_target_group" "ingress" {
  name                 = "${local.ingress_name}-${local.type}-nlb${local.deployment_suffix}-${random_string.nlb.result}"
  vpc_id               = var.networking["VpcId"]
  port                 = var.BACKEND_PORT
  protocol             = var.BACKEND_PROTOCOL
  deregistration_delay = var.DEREGISTRATION_DELAY

  dynamic "stickiness" {
    for_each = var.STICKINESS == true ? [1] : []
    content {
      type    = "source_ip"
      enabled = true
    }
  }

  health_check {
    protocol            = var.HTTP_HEALTH_CHECK_PROTOCOL
    path                = var.HEALTH_CHECK_URL
    port                = var.HEALTH_CHECK_PORT == null ? var.BACKEND_PORT : var.HEALTH_CHECK_PORT
    healthy_threshold   = 2
    unhealthy_threshold = 2
    interval            = 10
  }

  tags = merge(
    local.default_tags,
    map(
      "Name", "${local.environment_name}-${local.type}-nlb${local.deployment_suffix}",
      "Object", "aws_lb_target_group"
    )
  )
  lifecycle {
    create_before_destroy = true
  }
  depends_on = [aws_lb.ingress]
}

resource "aws_lb_target_group" "ingress_https" {
  count                = contains(values(var.ADDITIONAL_TCP_LISTENERS), "HTTPS") == true ? 1 : 0
  name                 = "${local.ingress_name}-${local.type}-nlb${local.deployment_suffix}-${random_string.https.result}"
  vpc_id               = var.networking["VpcId"]
  port                 = var.BACKEND_HTTPS_PORT
  protocol             = var.BACKEND_PROTOCOL
  deregistration_delay = var.DEREGISTRATION_DELAY

  dynamic "stickiness" {
    for_each = var.STICKINESS == true ? [1] : []
    content {
      type    = "source_ip"
      enabled = true
    }
  }

  health_check {
    protocol            = var.HTTPS_HEALTH_CHECK_PROTOCOL
    path                = var.HEALTH_CHECK_URL
    port                = var.HEALTH_CHECK_PORT == null ? var.BACKEND_HTTPS_PORT : var.HEALTH_CHECK_PORT
    healthy_threshold   = 2
    unhealthy_threshold = 2
    interval            = 10
  }

  tags = merge(
    local.default_tags,
    map(
      "Name", "${local.environment_name}-${local.type}-nlb${local.deployment_suffix}-${random_string.https.result}",
      "Object", "aws_lb_target_group"
    )
  )
  depends_on = [aws_lb.ingress]
}

resource "aws_lb_listener" "ingress_443" {
  count             = var.ENABLE_HTTPS_LISTENER ? 1 : 0
  load_balancer_arn = aws_lb.ingress.arn
  port              = 443
  protocol          = "TLS"
  ssl_policy        = var.BALANCER_SSL_POLICY
  certificate_arn   = var.BALANCER_CERTIFICATE_ARN

  default_action {
    target_group_arn = aws_lb_target_group.ingress.arn
    type             = "forward"
  }
  depends_on = [aws_lb_target_group.ingress]
}

resource "aws_lb_listener_certificate" "ingress_443" {
  count           = length(var.EXTRA_BALANCER_CERTIFICATE_ARN)
  listener_arn    = aws_lb_listener.ingress_443[0].arn
  certificate_arn = var.EXTRA_BALANCER_CERTIFICATE_ARN[count.index]
}

##
# Define ingress lb listener port 80 with no ssl redirect
##
resource "aws_lb_listener" "ingress_80_no_ssl_redirect" {
  count             = var.ENABLE_HTTPS_LISTENER == true || var.ENABLE_HTTP_LISTENER == false ? 0 : 1
  load_balancer_arn = aws_lb.ingress.arn
  port              = 80
  protocol          = "TCP" #tfsec:ignore:AWS004

  default_action {
    target_group_arn = aws_lb_target_group.ingress.arn
    type             = "forward"
  }
}

resource "aws_lb_listener" "ingress_additional_tcp_listener" {
  for_each          = var.ADDITIONAL_TCP_LISTENERS
  load_balancer_arn = aws_lb.ingress.arn
  port              = each.key
  protocol          = "TCP" #tfsec:ignore:AWS004

  default_action {
    target_group_arn = each.value == "HTTP" ? aws_lb_target_group.ingress.arn : aws_lb_target_group.ingress_https[0].arn
    type             = "forward"
  }
}
