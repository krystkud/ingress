terraform {
  required_version = ">= 0.12.0"

  required_providers {
    aws    = ">= 2.54"
    random = ">= 2.2"
  }
}
