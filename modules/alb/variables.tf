variable "MODULE_VERSION" {
  default     = "1.0.8"
  type        = string
  description = "Module version, update this value on commit to generate changelog and release"
}

locals {
  default_tags = {
    "Environment" = var.globals["Environment"]
    "Project"     = var.globals["Project"]
    "AccountName" = var.globals["AccountName"]
    "Repository"  = lookup(var.globals, "Repository", "")
    "Builder"     = lookup(var.globals, "Builder", "")
    "Module"      = "TFM/ingress/modules/alb:${var.MODULE_VERSION}"
    "CreatedBy"   = "terraform"
  }

  environment_name  = "${var.globals["Project"]}-${var.globals["Environment"]}"
  deployment_suffix = var.DEPLOYMENT_SUFFIX == "" ? "" : "-${lower(substr(var.DEPLOYMENT_SUFFIX, 0, min(5, length(var.DEPLOYMENT_SUFFIX))))}"
  ingress_name      = replace(substr(local.environment_name, 0, min(14, length(local.environment_name))), "_", "-")
  type              = var.INTERNAL == true ? "in" : "ex"
}

variable "globals" {
  description = "Provide global variables from common module"
  type        = map(any)
}

variable "networking" {
  description = "Provide common networking variables for other module"
  type        = map(any)
}

variable "PUBLIC_SUBNET_IDS" {
  description = "Provide list of the public subnet to launch resources in"
  type        = list(string)
}

variable "INGRESS_ACCESS_CIDR" {
  description = "Provide cidr block which should have access to ingress host"
  default     = ["0.0.0.0/0"]
  type        = list(string)
}

variable "BALANCER_IDLE_TIMEOUT" {
  description = "Provide the time in seconds that the connection is allowed to be idle."
  default     = 30
  type        = number
}

variable "BALANCER_SSL_POLICY" {
  description = "Provide name of the ssl policy"
  default     = "ELBSecurityPolicy-TLS-1-2-Ext-2018-06"
  type        = string
}

variable "BALANCER_CERTIFICATE_ARN" {
  description = "Provide arn of the acm certificate"
  default     = ""
  type        = string
}

variable "ENABLE_HTTPS_LISTENER" {
  description = "Define whether https listener should be enabled (required `BALANCER_CERTIFICATE_ARN`)"
  default     = true
  type        = bool
}

variable "EXTRA_BALANCER_CERTIFICATE_ARN" {
  description = "Provide a list of the acm certificates` arns "
  default     = []
  type        = list(string)
}

variable "BACKEND_PROTOCOL" {
  description = "Provide load balancer backend protocol, ex TLS, HTTP"
  default     = "HTTP"
  type        = string
}

variable "HEALTH_CHECK_PORT" {
  description = "Provide backend port, ex 8080"
  default     = "traffic-port"
  type        = string
}

variable "HEALTH_CHECK_MATCHER" {
  description = "Provide https responsonse code wthi will be treaded as ok"
  default     = "200-399"
  type        = string
}

variable "BACKEND_PORT" {
  description = "Provide backend port, ex 8080"
  default     = 1080
  type        = number
}

variable "HEALTH_CHECK_URL" {
  description = "Provide backend health check url"
  default     = "/healthz"
  type        = string
}

variable "INTERNAL" {
  description = "Provide if alb should be internal"
  default     = true
  type        = bool
}

variable "ACCESS_LOGS_BUCKET_NAME" {
  description = "Provide access logs bucket name"
  default     = ""
  type        = string
}

variable "ACCESS_LOGS_BUCKET_PREFIX" {
  description = "Provide access logs bucket prefix"
  default     = "ingress/alb"
  type        = string
}

variable "DEREGISTRATION_DELAY" {
  description = "Provide deregistration delay for Load Balancer Target Group."
  default     = 60
  type        = number
}

variable "DEPLOYMENT_SUFFIX" {
  description = "Provide optional deployment suffix name (5 chars) useful in A/B testing"
  default     = ""
  type        = string
}

variable "DISABLE_HTTP_LISTENER" {
  description = "Disable http port, possible only when https enabled"
  default     = false
  type        = bool
}

variable "DROP_INVALID_HEADER_FIELDS" {
  description = "Enable/disable dropping headers with invalid names. Dropping invalid header fields should be enabled in order to mitigate the risk of request smuggling attacks."
  default     = true
  type        = bool
}

variable "ENABLE_DELETION_PROTECTION" {
  description = "Enable this will prevent Terraform from deleting the load balancer"
  default     = false
  type        = bool
}

variable "ALB_ADDITIONAL_TAGS" {
  default     = {}
  description = "Additional tags for ALB"
  type        = map(any)
}

