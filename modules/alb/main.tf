##
# Generate random string
##
resource "random_string" "alb" {
  length  = 6
  special = false
  upper   = false

  keepers = {
    environment_name  = local.environment_name
    deployment_suffix = local.deployment_suffix
    vpc_id            = var.networking["VpcId"]
  }
}

##
# Define ingress lb
##
resource "aws_lb" "ingress" {
  name               = "${local.ingress_name}-${local.type}-alb${local.deployment_suffix}-${random_string.alb.result}"
  internal           = var.INTERNAL #tfsec:ignore:AWS005
  load_balancer_type = "application"
  idle_timeout       = var.BALANCER_IDLE_TIMEOUT
  subnets            = var.PUBLIC_SUBNET_IDS
  security_groups    = [aws_security_group.ingress.id]

  enable_deletion_protection       = var.ENABLE_DELETION_PROTECTION
  enable_cross_zone_load_balancing = true
  drop_invalid_header_fields       = var.DROP_INVALID_HEADER_FIELDS

  access_logs {
    enabled = var.ACCESS_LOGS_BUCKET_NAME == "" ? false : true
    bucket  = var.ACCESS_LOGS_BUCKET_NAME
    prefix  = var.ACCESS_LOGS_BUCKET_PREFIX
  }

  tags = merge(
    local.default_tags,
    map(
      "Name", "${local.environment_name}-${local.type}-alb${local.deployment_suffix}",
      "Object", "aws_lb"
    )
  )
  #checkov:skip=CKV_AWS_91:Logging is optional, it should be added on prod envs
}

##
# Define ingress target group port default 1080
##
resource "aws_lb_target_group" "ingress" {
  name   = "${local.ingress_name}-${local.type}-alb${local.deployment_suffix}-${random_string.alb.result}"
  vpc_id = var.networking["VpcId"]

  port                 = var.BACKEND_PORT
  protocol             = var.BACKEND_PROTOCOL
  deregistration_delay = var.DEREGISTRATION_DELAY

  stickiness {
    type    = "lb_cookie"
    enabled = false
  }

  health_check {
    path                = var.HEALTH_CHECK_URL
    port                = var.HEALTH_CHECK_PORT
    healthy_threshold   = 2
    unhealthy_threshold = 2
    timeout             = 2
    interval            = 5
    matcher             = var.HEALTH_CHECK_MATCHER
  }

  tags = merge(
    local.default_tags,
    map(
      "Name", "${local.environment_name}-${local.type}-alb${local.deployment_suffix}",
      "Object", "aws_lb_target_group"
    )
  )
  lifecycle {
    create_before_destroy = true
  }
  depends_on = [aws_lb.ingress]
}

##
# Define ingress lb listener port 443
##
resource "aws_lb_listener" "ingress_443" {
  count = var.ENABLE_HTTPS_LISTENER ? 1 : 0

  load_balancer_arn = aws_lb.ingress.arn
  port              = 443
  protocol          = "HTTPS"
  ssl_policy        = var.BALANCER_SSL_POLICY
  certificate_arn   = var.BALANCER_CERTIFICATE_ARN

  default_action {
    target_group_arn = aws_lb_target_group.ingress.arn
    type             = "forward"
  }
  depends_on = [aws_lb_target_group.ingress]
}

resource "aws_lb_listener_certificate" "ingress_443" {
  count           = length(var.EXTRA_BALANCER_CERTIFICATE_ARN)
  listener_arn    = aws_lb_listener.ingress_443[0].arn
  certificate_arn = var.EXTRA_BALANCER_CERTIFICATE_ARN[count.index]
}

##
# Define ingress lb listener port 80
##
resource "aws_lb_listener" "ingress_80" {
  count             = var.ENABLE_HTTPS_LISTENER && var.DISABLE_HTTP_LISTENER ? 0 : 1
  load_balancer_arn = aws_lb.ingress.arn
  port              = 80 
  protocol          = "HTTP" #tfsec:ignore:AWS004

  # if ssl force redirect to 443
  dynamic "default_action" {
    for_each = var.BALANCER_CERTIFICATE_ARN != "" ? list(1) : []
    content {
      type = "redirect"

      redirect {
        port        = 443
        protocol    = "HTTPS"
        status_code = "HTTP_301"
      }
    }
  }

  # if no ssl just forward
  dynamic "default_action" {
    for_each = var.BALANCER_CERTIFICATE_ARN == "" ? list(1) : []
    content {
      type             = "forward"
      target_group_arn = aws_lb_target_group.ingress.arn
    }
  }
  depends_on = [aws_lb_target_group.ingress]
  #checkov:skip=CKV_AWS_2:Listener 80 is used for redirect to 443 and it can be disabled
}
