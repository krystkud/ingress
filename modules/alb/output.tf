output "ingress_80_arn" {
  description = "Return ingress arn on port 80"
  value       = aws_lb_target_group.ingress.arn
}

output "ingress_dns_name" {
  description = "Return ingress dns name"
  value       = aws_lb.ingress.dns_name
}

output "ingress_zone_id" {
  description = "Return ingress zone id"
  value       = aws_lb.ingress.zone_id
}

output "ingress_to_nodes_security_group" {
  description = "Return ingress to node security group id"
  value       = aws_security_group.ingress_to_nodes.id
}

output "ingress_security_group" {
  description = "Return ingress security group id"
  value       = aws_security_group.ingress.id
}

output "ingress_arn" {
  description = "Return ingress arn"
  value       = aws_lb.ingress.arn
}