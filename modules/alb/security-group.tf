##
# Define ingress security group
##
resource "aws_security_group" "ingress" {
  name        = "${local.environment_name}-${local.type}${local.deployment_suffix}-${random_string.alb.result}-alb"
  vpc_id      = var.networking["VpcId"]
  description = "${local.environment_name}-${local.type}${local.deployment_suffix}-${random_string.alb.result}-alb-sg"

  tags = merge(
    local.default_tags,
    var.ALB_ADDITIONAL_TAGS,
    map(
      "Name", "${local.environment_name}-${local.type}${local.deployment_suffix}-${random_string.alb.result}-alb",
      "Object", "aws_security_group"
    )
  )
}

resource "aws_security_group_rule" "ingress_80" {
  count             = var.ENABLE_HTTPS_LISTENER && var.DISABLE_HTTP_LISTENER ? 0 : 1
  description       = "Allow incoming network flow on port 80/tcp"
  type              = "ingress"
  from_port         = 80
  to_port           = 80
  protocol          = "tcp"
  cidr_blocks       = var.INGRESS_ACCESS_CIDR #tfsec:ignore:AWS006
  security_group_id = aws_security_group.ingress.id
}

resource "aws_security_group_rule" "ingress_443" {
  description       = "Allow incoming network flow on port 443/tcp"
  type              = "ingress"
  from_port         = 443
  to_port           = 443
  protocol          = "tcp"
  cidr_blocks       = var.INGRESS_ACCESS_CIDR #tfsec:ignore:AWS006
  security_group_id = aws_security_group.ingress.id
}

##
# Define ingress to nodes security group
##
resource "aws_security_group" "ingress_to_nodes" {
  name        = "${local.environment_name}-${local.type}${local.deployment_suffix}-${random_string.alb.result}-alb-to-nodes"
  vpc_id      = var.networking["VpcId"]
  description = "${local.environment_name}-${local.type}${local.deployment_suffix}-${random_string.alb.result}-alb-to-nodes-sg"

  tags = merge(
    local.default_tags,
    map(
      "Name", "${local.environment_name}-${local.type}${local.deployment_suffix}-${random_string.alb.result}-alb-to-nodes",
      "Object", "aws_security_group"
    )
  )
}

resource "aws_security_group_rule" "ingress_to_nodes_ingress_flow" {
  description              = "Allow incoming network flow from ingress to worker nodes/tcp"
  type                     = "ingress"
  from_port                = var.BACKEND_PORT
  to_port                  = var.BACKEND_PORT
  protocol                 = "tcp"
  source_security_group_id = aws_security_group.ingress.id
  security_group_id        = aws_security_group.ingress_to_nodes.id
}

resource "aws_security_group_rule" "ingress_egress_flow" {
  type                     = "egress"
  description              = "Allow alb to communicate with worker nodes/tcp"
  from_port                = var.BACKEND_PORT
  to_port                  = var.BACKEND_PORT
  protocol                 = "tcp"
  source_security_group_id = aws_security_group.ingress_to_nodes.id
  security_group_id        = aws_security_group.ingress.id
}

resource "aws_security_group_rule" "ingress_to_nodes_ingress_health_check_flow" {
  count = var.HEALTH_CHECK_PORT != "traffic-port" ? 1 : 0

  description              = "Allow incoming network flow from ALB health check to worker nodes/tcp"
  type                     = "ingress"
  from_port                = var.HEALTH_CHECK_PORT
  to_port                  = var.HEALTH_CHECK_PORT
  protocol                 = "tcp"
  source_security_group_id = aws_security_group.ingress.id
  security_group_id        = aws_security_group.ingress_to_nodes.id
}

resource "aws_security_group_rule" "ingress_egress_health_check_flow" {
  count = var.HEALTH_CHECK_PORT != "traffic-port" ? 1 : 0

  type                     = "egress"
  description              = "Allow alb to communicate with worker nodes/tcp on health check port"
  from_port                = var.HEALTH_CHECK_PORT
  to_port                  = var.HEALTH_CHECK_PORT
  protocol                 = "tcp"
  source_security_group_id = aws_security_group.ingress_to_nodes.id
  security_group_id        = aws_security_group.ingress.id
}
