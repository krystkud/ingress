# Change Log
-------------
## 1.1.1 2021-08-30
TM-467: Added Jenkinsfile.

## 1.1.0 13.07.2021
TM-457: Fix issue with missing security group rules when custom health check port is used

## 1.0.9 15.04.2021
TM-388: Sg and healthcheck port as variable 

## 1.0.8 05.03.2021
TM-347: Implement deletion protection for ingress alb/nlb

## 1.0.7 17-02-2021
TM-325: Deprecate Aggregators module
TM-324: CD-429 Modify a way how listeners are created in NLB ingress
TM-309: More meaningful tags in modules

## 1.0.6 2020-01-18
TM-301: Fix issue with Stickiness Block

## 1.0.5 2020-01-18
TM-279: Bump AWS provider
TM-290 Fix comments only

## 1.0.4 2020-01-04
TM-279 Unlock AWS provider

## 1.0.3 2020-12-22

TM-278 1.0.3 Allow dropping invalid headers in ALB

## 1.0.2 2020-12-04

TM-252 1.0.2 Release and master\_12 cleanup

## 1.0.1 2020-12-03

TM-242 Disable 80 ingress security group rule when HTTP is disabled
TM-222 Add posibility to disable http port on ALB (requested by SRC-2301)

## 1.0.0 2020-09-03

TM-135 replace character in ingress name
TM-108 delete lifecycle rule from SG rules in ALB
TM-110 Set INTERNAL true by default
TM-22  Added tests for ingress module.

## 0.1.1 2020-08-13
Added tests

## 0.1.0 2020-07-29
Initial package

#### Added
- Initial package for Ingress
